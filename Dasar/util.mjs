import util from 'util'

const firstName = 'Ridwan'
const lastName = 'Hasanah'

console.info(`Hello ${firstName} ${lastName}`)
console.info(util.format('Hello %s %s',firstName,lastName))

const person = {
  firstName: 'Ridwan',
  lastName: 'Hasanah'
}

console.info(`Person : `,JSON.stringify(person))
console.info(util.format('Person :  %j',person))