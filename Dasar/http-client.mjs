import http from 'http'
// const endpoint = 'https://eo20c4vvv7ohdm5.m.pipedream.net'
const endpoint = 'http://localhost:3000'
const request = http.request(endpoint, {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
}, (response) => {
  response.addListener('data', (data) => {
    console.info(`Receive data : ${data.toString()}`)
  })
})
const body = JSON.stringify({
  firstName : 'Ridwan',
  lastName : 'Hasanah'
})
request.write(body)
request.end()