import net from 'net'

const client = net.createConnection({port: 3000,host: 'localhost'})

client.addListener('data',(data)=>{
  console.info(`Receive data from server : ${data.toString()}`)
})

setInterval(()=>{
  client.write(`${process.argv[2]} ` + new Date() + '\r\n')
},2000)

//Jalankan dengan node net-client argumen contoh : node net-client.mjs Ridwan