import {URL} from 'url'

const pzn = new URL('https:programmerzamannow.com/belajar?kelas=nodejs')

console.info(`toString ${pzn.toString()}`)
console.info(`href ${pzn.href}`)
console.info(`protocol ${pzn.protocol}`)
console.info(`host ${pzn.host}`)
console.info(`pathname ${pzn.pathname}`)
console.info(`searchParams ${pzn.searchParams}`)

console.info('\nUbah URL\n')

pzn.host = 'google.com'
pzn.searchParams.append('status','Freemium')

console.info(`toString ${pzn.toString()}`)
console.info(`href ${pzn.href}`)
console.info(`protocol ${pzn.protocol}`)
console.info(`host ${pzn.host}`)
console.info(`pathname ${pzn.pathname}`)
console.info(`searchParams ${pzn.searchParams}`)