export const sayHelloAsync = (name) => {
  return new Promise((resolve,reject)=>{
    setTimeout(() => {
      if(name){
        resolve(`Hai ${name}`)
      }else{
        reject('Name is Empty')
      }
    },1000)
  })
}