import { sayHelloAsync } from "../src/async"

test('test async function', async ()=>{
  const result = await sayHelloAsync('Ridwan');

  expect(result).toBe('Hai Ridwan');
})

test('tesct async mather', async ()=>{
  await expect(sayHelloAsync('Ridwan')).resolves.toBe('Hai Ridwan')
  await expect(sayHelloAsync()).rejects.toBe('Name is Empty')
})