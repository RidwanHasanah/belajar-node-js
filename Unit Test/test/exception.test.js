import { MyException, callMe } from "../src/exception"

test('exception',()=>{
  expect(()=> callMe('Ridwan')).toThrow()
  expect(()=> callMe('Ridwan')).toThrow(MyException)
  expect(()=> callMe('Ridwan')).toThrow('Ups my exceptions happens')
})