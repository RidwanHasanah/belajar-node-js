test('string.not',()=>{
  const name = 'Ridwan Hasanah';
  expect(name).not.toBe('Daud');
  expect(name).not.toEqual('Lukman');
  expect(name).not.toMatch(/Hanif/);
})

test('number.not',()=>{
  const number = 3 + 3;
  expect(number).not.toBeGreaterThan(7);
  expect(number).not.toBeLessThan(5);
  expect(number).not.toBe(8);
})