test('array simple',()=>{
  const names = ['ridwan','hasanah'];
  expect(names).toEqual(['ridwan','hasanah']);
  expect(names).toContain('ridwan')
})

test('array object',()=>{
  const persons = [
    {'name' : 'Ridwan'},
    {'name' : 'Hasanah'}
  ]

  expect(persons).toContainEqual({
    'name' : 'Hasanah'
  })
})